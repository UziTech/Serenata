<?php

namespace Serenata\Indexing;

/**
 * Enumeration of workspace event names.
 */
class WorkspaceEventName
{
    /**
     * @var string
     */
    public const CHANGED = 'workspaceChanged';
}
